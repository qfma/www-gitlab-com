---
layout: markdown_page
title: "E-group offsite"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

The E-group offsite happens every quarter for three days.

## Goal

The goal is to have 25 or 50 minute discussions around topics that are:

1. Top of mind
1. Actionable
1. Impactful to the trajectory of the company
1. Cross-functional

## Notes

We take notes in a Google Doc that contains (a copy of) the agenda.
Please add links to relevant materials and proposals up front.
When there is an issue or doc linked we take notes there instead of in the overall doc.

## Followup

We reserve 1/3 of the time for each subject to do the followup.
Before we did this many conclusions never landed and/or resulted into action.
This can take the form of:

1. Merge request to the handbook
1. Create an issue
1. Schedule a meeting
1. Share notes with the rest of the company in Slack
1. Etc.

## Time management

If we can conclude a topic early we move on to one from a reserve list.
For each part of the day there is a chairperson who moves on, switches to followup, and leads other adjustments.

## Logistics

Since most of the E-group is in the bay area we'll go to a location that is drivable or a short direct flight, for example: East Bay, Denver, Sonoma. We tend to pick a location at or close to a member of the group.

## Timeline

1. Plan
1. Gathering subjects

## Organization

The executive assistant for the CEO is responsible for organizing this.
