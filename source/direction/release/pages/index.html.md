---
layout: markdown_page
title: "Category Vision - GitLab Pages"
---

- TOC
{:toc}

## GitLab Pages

GitLab Pages allows you to create a statically generated website from your project that
is automatically built using GitLab CI and hosted on our infrastructure.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=pages&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Please reach out to PM Jason Lenny ([E-Mail](mailto:jason@gitlab.com) /
[Twitter](https://twitter.com/j4lenn)) if you'd like to provide feedback or ask
questions about what's coming.

### Overall Prioritization

Pages is not strategically our most important Release feature, but it's a popular
feature and one that people really enjoy engaging with as part of the GitLab
experience; it's truly one of our most "personal" features in the Release stage.
We do not plan to provide a market-leading solution for static web page hosting,
but we do want to offer one that is capable for most basic needs, in particular
for hosting static content and documentation that is a part of your software
release.

## What's Next & Why

Automatic certificate renewal ([gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996))
is our most popular issue and one that can be quite irritating to manually
manage. We're excited to address this next in order to make using Pages
easier and require less ongoing maintenance.

## Competitive Landscape

Automatic certificate renewal ([gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996))
is highly popular in almost every category here, and would make adoption
of GitLab Pages much more attractive.

## Top Customer Success/Sales Issue(s)

The most popular customer issue is [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996),
which makes setting up and renewing certificates automatic for your Pages site.
This is an ongoing hassle once you set up your Pages site since renewals in
particular are frequent and manual. Solving this problem will make Pages
even more automatic and easy to use.

## Top Customer Issue(s)

Aligned with what CS is seeing, [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate
renewal) is our top customer issue.

## Top Internal Customer Issue(s)

Similar to many other categories in the vision here at the moment, internal
customers have most frequently raised [gitlab-ce#28996](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) (certificate
renewal) as the biggest irritation in working with pages.

## Top Vision Item(s)

From a vision standpoint, adding Review Apps for Pages ([gitlab-ce#26621](https://gitlab.com/gitlab-org/gitlab-ce/issues/26621))
is interesting because it allows for more sophisticated development flows
involving testing, where at the moment the only environment that GitLab
understands is production. This would level up our ability for Pages to
be a more mission-critical part of projects and groups.
